package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

	// 商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";

	// 商品別集計ファイル名
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST = "定義ファイルが存在しません";
	private static final String FILE_INVALID_FORMAT = "定義ファイルのフォーマットが不正です";
	private static final String FILE_NOT_SEQUENCE = "売上ファイル名が連番になっていません";
	private static final String SALEAMOUNT_OVER = "合計金額が10桁を超えました";
	private static final String BRANCH_CODE_NO_EXIST = "の支店コードが不正です";
	private static final String COMMODITY_CODE_NO_EXIST = "の商品コードが不正です";
	private static final String SALESFILE_INVALID_FORMAT = "のフォーマットが不正です";

	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {
		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();

		// 商品コードと商品名を保持するMap
		Map<String, String> commodityNames = new HashMap<>();
		// 商品コードと売上金額を保持するMap
		Map<String, Long> commoditySales = new HashMap<>();

		//コマンドライン引数が1つ設定されているか確認
		if (args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}

		// 支店定義ファイル読み込み処理
		if (!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales, "支店", "[0-9]{3}")) {
			return;
		}
		
		// 商品定義ファイル読み込み処理
		if (!readFile(args[0], FILE_NAME_COMMODITY_LST, commodityNames, commoditySales, "商品", "[0-9a-zA-Z]{8}")) {
			return;
		}

		// ※ここから集計処理を作成してください。(処理内容2-1、2-2)
		//すべてのファイルを取得
		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();

		for (int i = 0; i < files.length; i++) {
			//読み込まれたものがファイルでファイル名が数字の8桁かどうか確認
			if (files[i].isFile() && files[i].getName().matches("[0-9]{8}.+rcd$")) {
				rcdFiles.add(files[i]);
			}
		}

		//売上ファイルが連番か確認
		Collections.sort(rcdFiles);
		for (int i = 0; i < rcdFiles.size() - 1; i++) {
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0, 8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0, 8));

			if ((latter - former) != 1) {
				System.out.println(FILE_NOT_SEQUENCE);
				return;
			}
		}

		BufferedReader rcdBr = null;

		String rcdLine;

		for (int i = 0; i < rcdFiles.size(); i++) {

			try {
				FileReader rcdFr = new FileReader(rcdFiles.get(i));
				rcdBr = new BufferedReader(rcdFr);
				ArrayList<String> rcdItems = new ArrayList<>();

				while ((rcdLine = rcdBr.readLine()) != null) {
					rcdItems.add(rcdLine);
				}

				//売上ファイルの中身が3行か確認
				if (rcdItems.size() != 3) {
					System.out.println(rcdFiles.get(i) + SALESFILE_INVALID_FORMAT);
					return;
				}

				//売上金額が数字なのか確認
				if (!rcdItems.get(2).matches("^[0-9]*$")) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}
				
				long fileSale = Long.parseLong(rcdItems.get(2));

				//売上ファイルの支店コードが支店定義ファイルに存在するか確認
				if (!branchNames.containsKey(rcdItems.get(0))) {
					System.out.println(rcdFiles.get(i) + BRANCH_CODE_NO_EXIST);
					return;
				}

				//売上ファイルの商品コードが商品定義ファイルに存在するか確認
				if (!commodityNames.containsKey(rcdItems.get(1))) {
					System.out.println(rcdFiles.get(i) + COMMODITY_CODE_NO_EXIST);
					return;
				}

				//Mapから取得した売上金額を支店別に加算する
				Long branchSaleAmount = branchSales.get(rcdItems.get(0)) + fileSale;

				//Mapから取得した売上金額を商品別に加算する
				Long commoditySaleAmount = commoditySales.get(rcdItems.get(1)) + fileSale;

				//それぞれの売上金額が10桁を超えないか確認			
				if ((branchSaleAmount >= 10000000000L) || (commoditySaleAmount >= 10000000000L)) {
					System.out.println(SALEAMOUNT_OVER);
					return;
				}

				branchSales.put(rcdItems.get(0), branchSaleAmount);
				commoditySales.put(rcdItems.get(1), commoditySaleAmount);

			} catch (IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;
			} finally {
				// ファイルを開いている場合
				if (rcdBr != null) {
					try {
						// ファイルを閉じる
						rcdBr.close();
					} catch (IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}
				}
			}
		}

		// 支店別集計ファイル書き込み処理
		if (!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}

		// 商品別集計ファイル書き込み処理
		if (!writeFile(args[0], FILE_NAME_COMMODITY_OUT, commodityNames, commoditySales)) {
			return;
		}

	}

	/**
	 * 支店定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 読み込み可否
	 * 
	 * 
	 * 商品定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 商品コードと商品名を保持するMap
	 * @param 商品コードと売上金額を保持するMap
	 * @return 読み込み可否
	 * 
	 */
	private static boolean readFile(String path, String fileName, Map<String, String> Names,
			Map<String, Long> Sales, String kind, String judge) {
		BufferedReader br = null;

		try {
			File file = new File(path, fileName);

			//支店定義ファイル+商品定義ファイルの存在確認	
			if (!file.exists()) {
					System.out.println(kind + FILE_NOT_EXIST);
					return false;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			//Mapに値を保持する
			String line;
			// 一行ずつ読み込む
			while ((line = br.readLine()) != null) {
				// ※ここの読み込み処理を変更してください。(処理内容1-2)
				String[] items = line.split(",");

				//支店定義ファイル+商品定義ファイルのフォーマット確認
				if ((items.length != 2) ||(!items[0].matches(judge))){
						System.out.println(kind + FILE_INVALID_FORMAT);
						return false;
				}

				Names.put(items[0], items[1]);
				Sales.put(items[0], 0L);

			}

		} catch (IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if (br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch (IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 支店別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 * 
	 * 
	 * 商品別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 商品コードと商品名を保持するMap
	 * @param 商品コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> Names,
			Map<String, Long> Sales) {
		// ※ここに書き込み処理を作成してください。(処理内容3-1)
		BufferedWriter bw = null;
		try {
			File file = new File(path, fileName);
			FileWriter fr = new FileWriter(file);
			bw = new BufferedWriter(fr);

			for (String key : Names.keySet()) {

				bw.write(key + "," + Names.get(key) + "," + Sales.get(key));
				//改行する
				bw.newLine();
			}

		} catch (IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if (bw != null) {
				try {
					// ファイルを閉じる
					bw.close();
				} catch (IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}

		return true;
	}

}
